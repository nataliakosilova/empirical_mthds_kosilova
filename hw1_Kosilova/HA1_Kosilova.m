%Empirical methods, homework 1

diary h1.txt
%Problem 1
display('Problem 1')
X=[1, 1.5, 3,4,5,7,9,10];
Y1=-2+0.5*X;
Y2=2+0.5*X.^2;

plot(X,Y1,'r-', X, Y2, 'b-');
legend('Y1','Y2')

%Problem 2
display('Problem 2')
st=(20+10)/199;
x=-10:st:20;
sum_x=sum(x)

%Problem 3
display('Problem 3')
A=[2 4 6; 1 7 5; 3 12 4]
B=[-2; 3; 10]
C=A'*B
D=(A'*A)^(-1)*B
E=sum(sum(C))
F=A([1,3], [1:2])

y=linsolve(A,B)
%if we are not allowed to use linsolve, since detA<>0, the matrix is
%invertible and aquare so we could solve as
det(A)
y=(A)^(-1)*B



%Problem 4
display('Problem 4')
Z=zeros(3,3);
% check out answer key for use of kron() operator
B=blkdiag(A,A,A,A,A)

%Problem 5
display('Problem 5')
A=10+5*randn(5,3)
[I]=find(A<10);
A=ones(5,3);
A(I)=0
% try just A = A<10

%Problem 6
display('Problem 6')
A=importdata('datahw1.csv');
id=A(:,1);
year=A(:,2);
ex=A(:,3);
rd=A(:,4);
prod=A(:,5);
cap=A(:,6);

%combining vector of regressors
X=[ex, rd, cap];
B=fitlm(X, prod)

%if we are not allowed to use fitlm:
%adding constant
n=length(prod);
x=[ones(n,1) X];
y=regress(prod, x)
%compute the first part of variance

sumx=zeros(4);
for i=1:n
    sumx=sumx+x(i,:)'*x(i,:);
end
sample=(1/n*sumx)^(-1);

%now we have to estimate E[xx' eps^2]
%let's compute residuals
res=prod-x*y;
res_sq=res.^2;

sum2=zeros(4);
for i=1:n
    sum2=sum2+x(i,:)'*x(i,:)*res_sq(i);
end
sample2=(1/n)*sum2;
   
var_het=sample*sample2*sample

se_het=zeros(4,1);
for i=1:4
    se_het(i,1)=(var_het(i,i)/n)^(1/2);
end

display('heteroskedastic standard errors:')
se_het

%under the homoskedasticity assumption we may just proceed as follow:
%estimate E[eps^2] under assumption that it doesn't depend on x

eps2_hat=1/n*sum(res_sq);
var_hom=sample*eps2_hat

se_hom=zeros(4,1);
for i=1:4
    se_hom(i,1)=(var_hom(i,i)/n)^(1/2);
end

%This is our errors under the homoskedasticity assumption, and they coincide with what matlab gets
%with built-in fitlm function
display('homoskedastic standard errors:')
se_hom

%I'll work with the heteroskedastic errors to compute t-statistics and
%p-values

display('t-statistics:')
tstat=y./se_het

display('p-values:')
pval=2*(1-tcdf(abs(tstat),n-4))

diary off
