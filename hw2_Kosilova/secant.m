function xx  = secant(f,x)
%Uses secant method to find a root of the function f
%  Inputs: function f, 1 by 1
%          starting point x, 1 by 1
%  Outputs: evalueted root x, 1 by 1
%           flag=0, root converges
%           flag=2, max iterations exceeded


maxsteps=100;
eps=0.001;

x_new=x;
x_old=x+10*eps;


for t=1:maxsteps
    
    x_int=x_new;
    x_new=x_old-f(x_old)*(x_new-x_old)/(f(x_new)-f(x_old));
    
if norm(x_new-x_int)<eps
    flag=0;
    xx=x_new;
    return
end

x_old=x_int;
xx=x_new;

end

if t==maxsteps
    flag=1;
    warning('Failure to converge: secant method');
end


return  


