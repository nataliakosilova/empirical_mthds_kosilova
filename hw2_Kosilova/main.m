% please use the sections breaks : double percent symbols. I incerted those
% here 



% Empirical Methods, HW2 by Natalia Kosilova, 09.13.2017
% Discrete Choice Demand Pricing Equilibrium

% In this code I use four additional functions - demand, func, secant and
% temp (all these can be found in the hw_2 folder)
%
%   1) demand takes prices as input and returns 3-by-1 vector of demand for
%      products A, B and C under these prices. (The demand for outside option
%      can be found as 1-sum(q_i)), but as I don't need it to compute
%      equilibrium, I just commented that line.
%   2) func takes 3-by-1 vector of starting points and calculates for each firm the value
%      of the derivative of the profit with respect to its price. (In eqm
%      we want this to be zero).
%   3) secant function and a scalar starting point and returns the
%      approximate root of that function that was found by the secant method.
%   4) temp - auxiliary function, used in Gauss-Jacobi method. It's
%      convenient to use it for the diagonal elements of the Jacobian


% always have cleaning the variable set
clean all

%% I also wanted to collect all the number of iterations (and time too) for each method in a
%matrix, so that it will be easy to compare methods in the end.
It=zeros(4,4);
Time=zeros(4,4);

%set max number of iterations to be 100 for each method and the precision
%level to be eps
%To compare the efficiency of each method I want to use the same
%convergence criterion
maxsteps=100;
eps=0.000001;


%define the matrix of starting points 
x_init=[1, 1, 1; 0,0,0;0,1,2; 3,2,1]';


%% Part 1
%Check the demand under all the prices=1

in_x1=[1,1,1]';
display('Demand for each option when pi=1 for each i:')
d=demand(in_x1)
% here should be one = sign, not ==


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Part 2 
display('Broyden method:')
for i=1:4
    tic
[x,P,flag,it]=broyden(@(P)func(P), x_init(:,i))
% set the same convergence tolerance here as you use everywhere else. (eps)
q=zeros(3,1);
It(1,i)=it;
Time(1,i)=toc;
if flag==0 
q(:)=demand(x)
% what did x1 mean? it was giving an error message. don't commit code that
% produces error messages when run from the blank variables set.
end
end 

 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Part 3
%Gauss-Jacobi method
display('Gauss-Jacobi method')
for i=1:4
    tic
p_new=x_init(:,i);
flag=0; n=1; t=0;
while n>eps
       t=t+1;
       p_old=p_new;
       p_new(1)=secant(@(p1) temp(p1,p_old(2), p_old(3)) , p_old(1));
       p_new(2)=secant(@(p1) temp(p1,p_old(1), p_old(3)) , p_old(2));
       p_new(3)=secant(@(p1) temp(p1,p_old(1), p_old(2)) , p_old(3));
       n=norm(p_new-p_old);
       if  t>maxsteps
           return
        warning('Failure to converge');
        flag=1;
        return; 
       end
end
display('starting point: ')
st=x_init(:,i)
display('solution: ')
p_new
display('number of iterations: ')
t
It(2,i)=t;
Time(2,i)=toc;
if flag==0
    display('Convergence succesfull')
end
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Part 4
display('p=1/(1-q(p) updating rule')

for i=1:4
    tic
p_new=x_init(:,i);
n=1; t=0; flag=0;
while n>eps
    t=t+1;
    p_old=p_new;
    q(:)=demand(p_new);
    p_new=1./(1-q);
    n=norm(p_new-p_old)
        if t>maxsteps
            warning('Failure to converge');
            flag=1;
            return;
        end
end
display('starting point: ')
st=x_init(:,i)
display('solution: ')
p_new
display('number of iterations: ')
t
It(3,i)=t;
Time(3,i)=toc;
if flag==0
    display('Convergence succesfull')
end
end

 
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %% Part 5
 %only single secant step in each iteration
display('Part 5')
 for i=1:4
     tic
p_new=x_init(:,i);
flag=0;
n=1;
t=0;

while n>eps
       t=t+1;
       p_old=p_new;
       x0=p_old+100*eps;
       p_new(1)=x0(1)-temp(x0(1),x0(2),x0(3))*(p_old(1)-x0(1))/(temp(p_old(1),p_old(2), p_old(3))-temp(x0(1),x0(2), x0(3)));
       p_new(2)=x0(2)-temp(x0(2),x0(1),x0(3))*(p_old(2)-x0(2))/(temp(p_old(2),p_old(1), p_old(3))-temp(x0(2),x0(1), x0(3)));
       p_new(3)=x0(3)-temp(x0(3),x0(2),x0(1))*(p_old(3)-x0(3))/(temp(p_old(3),p_old(2), p_old(1))-temp(x0(3),x0(2), x0(1)));

       n=norm(p_new-p_old);
       if  t>maxsteps
        warning('Failure to converge');
        flag=1;
        return; 
       end
end
display('starting point: ')
st=x_init(:,i)
display('solution: ')
p_new
display('number of iterations: ')
t
It(4,i)=t;
Time(4,i)=toc;
if flag==0
    display('Convergence succesfull')
end
end
 
  
%% Matrices with time and number of iterations:
 It   
 Time
 
 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Part 6
% I also did Gauss-Seidel method just for fun, to see if maybe it works
% faster. It seems to be faster than Broyden and Gauss-Jacob, but of a
% comparable speed with the last to methods we used. 

% display('Gauss-Seidel with single secant step')
%  GS_It=zeros(1,4);
%  GS_Time=zeros(1,4);
% 
% for i=1:4
%     tic
% p_new=x_init(:,i);
% flag=0;
% n=1;
% t=0;
% %The following matrix is helpful with indexes
% M=[2,3; 1,3; 1,2];
% 
% while n>eps
%        t=t+1;
%        p_old=p_new;
%        j=mod(t+2,3)+1;
%        p_new(j)=secant(@(p1) temp(p1,p_old(M(j,1)), p_old(M(j,2))) , p_old(j));
%        n=norm(p_new-p_old);
%        if  t>maxsteps
%         warning('Failure to converge');
%         flag=1;
%         return; 
%        end
% end
% display('starting point: ')
% st=x_init(:,i)
% display('solution: ')
% p_new
% display('number of iterations: ')
% t
% GS_It(1,i)=t;
% GS_Time(1,i)=toc;
% if flag==0
%     display('Convergence succesfull')
% end
% end
% GS_It
% GS_Time
   