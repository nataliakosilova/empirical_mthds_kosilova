function q = demand(P)
%defining the demand function for the three products and outside option
%Let's normalize each vi to 1 
va=-1; vb=-1; vc=-1;

q(1)=exp(va-P(1))/(1+exp(va-P(1))+exp(vb-P(2))+exp(vc-P(3)));
q(2)=exp(vb-P(2))/(1+exp(va-P(1))+exp(vb-P(2))+exp(vc-P(3)));
q(3)=exp(vc-P(3))/(1+exp(va-P(1))+exp(vb-P(2))+exp(vc-P(3)));
%q0=1/(1+exp(va-pa)+exp(vb-pb)+exp(vc-pc));
end

