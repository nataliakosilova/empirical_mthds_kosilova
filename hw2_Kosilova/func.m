function f = func(P)
%Firms maximize their utility by simultaneously setting prices.
%Max pi*qi by pi, the equilibrium system will be of the form
%qi*(1-pi*(1-q_i))=0 for i=A,B,C
%where each qi is defined in the function "demand"

va=-1;,vb=-1; vc=-1;

% f(1,1)=exp(va-P(1))/(1+exp(va-P(1))+exp(vb-P(2))+exp(vc-P(3)))*(1-P(1)*(1-exp(va-P(1))/(1+exp(va-P(1))+exp(vb-P(2))+exp(vc-P(3)))));
% f(2,1)=exp(va-P(2))/(1+exp(va-P(1))+exp(vb-P(2))+exp(vc-P(3)))*(1-P(2)*(1-exp(va-P(2))/(1+exp(va-P(1))+exp(vb-P(2))+exp(vc-P(3)))));
% f(3,1)=exp(va-P(3))/(1+exp(va-P(1))+exp(vb-P(2))+exp(vc-P(3)))*(1-P(3)*(1-exp(va-P(3))/(1+exp(va-P(1))+exp(vb-P(2))+exp(vc-P(3)))));


f(1,1)=(1-P(1)*(1-exp(va-P(1))/(1+exp(va-P(1))+exp(vb-P(2))+exp(vc-P(3)))));
f(2,1)=(1-P(2)*(1-exp(va-P(2))/(1+exp(va-P(1))+exp(vb-P(2))+exp(vc-P(3)))));
f(3,1)=(1-P(3)*(1-exp(va-P(3))/(1+exp(va-P(1))+exp(vb-P(2))+exp(vc-P(3)))));



end

