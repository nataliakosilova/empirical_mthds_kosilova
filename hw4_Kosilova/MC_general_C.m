function [Like]= MC_general_C(par, X, Y, Z, ub)

Func=@(x)(1+exp(-x)).^(-1);
beta_0=par(1);
sigma_beta=par(2);
gamma=par(3);

u_0=par(4);
sigma_beta_u=par(5);
sigma_u=par(6);

R=[sigma_beta sigma_beta_u; sigma_beta_u sigma_u];
% good practice is to treat chol of the matrix as parameter in order to
% avoid taking costly decomposition inside the functions. helps with large
% matrices

% I noticed that fmincon does not always satisfy proposed constraints. 
if det(R)<=0
    Like=1e10;
    return;
end

mu=[beta_0 u_0];
n=100;
z=zeros(100,2,n);
for p=1:n
z(:,:,p)=repmat(mu,100,1)+ub(:,:,p)*chol(R);
end

U=z(:,2,:);
BETA=z(:,1,:);
L_i=@(beta, u, gamma) prod((Func(ones(size(X,1),1)*beta.*X+gamma*Z+ones(size(X,1),1)*u)).^Y.*(1-Func(ones(size(X,1),1)*beta.*X+gamma*Z+ones(size(X,1),1)*u)).^(1-Y),1);
L_int=zeros(n,100);

for i=1:n
L_int(i,:)=(L_i(BETA(i,:), U(i,:), gamma));
end
   
Like=-sum(log(mean(L_int,1)));
      
end