% Empirical Methods, HW4
% By Natalia Kosilova
% Last update: December 10

%% Importing Data
clear all;
A=importdata('hw4data.mat');
N=A.N;
T=A.T;
X=A.X;
Y=A.Y;
Z=A.Z;

%% Data Specification
beta_0=0.1;
sigma_beta=1;
gamma=0;
in_par=[beta_0, sigma_beta, gamma];
%Assume u=0 for the first part
u=zeros(1,100);

%% Part 1. 
% Gaussian quadrature

% I have found a function that computes nodes and weigths for Gauss-Hermite
% quadrature of order n>1.
[nodes,w]=GaussHermite_2(20);

display('Likelihood function calculated with Gaussian Quadrature')
GQ_likelihood=GQ(in_par, X, Y, Z, u, nodes ,w)


%% Part 2. Monte Carlo Integration

% Generate n draws from N(0,1)
% Instead of saving the seed to create random variables for each value of mean and standard deviation, I create and save 
% (n,100) normal values, where n -- number of nodes to be used in
% Monte_Carlo method. I create N(beta_0, sigma_beta) inside the MC_general_B
% function

%
n=100;
BET=randn(n,100);

display('Likelihood function calculated with Monte-Carlo Integration')
MC_Likelihood=MC_general_B(in_par, u, X, Y, Z, BET)

%% Part 3. 
% a) Monte Carlo integration


 options1 = optimset('Display','iter','TolFun',1e-10,'TolX',1e-12,'MaxFunEvals',10000);
 x0=[0.1;1.1;0.1];


[P_MC_1, fval, exitflag, output]=fmincon(@(par) MC_general_B(par, u, X, Y, Z, BET), x0,[],[],[],[],[-inf; 0; -inf],[],[],options1);
  display('(Monte-Carlo) ML Estimation')
  display('Starting point')
 x0
 display('argmax')
 display('beta_0, sigma_beta, gamma=')
 P_MC_1
 display('maximized value of loglikelihood function')
 -fval

% b) Gaussian Quadrature

[P_GQ, fval, exitflag, output]=fmincon(@(par) GQ(par, X, Y, Z, u, nodes ,w), x0,[],[],[],[],[-inf; 0; -inf],[],[],options1);
  display('(Gaussian Quadrature) ML Estimation')
  display('Starting point') 
 x0
 display('argmax')
 display('beta_0, sigma_beta, gamma=')
 P_GQ
 display('maximized value of loglikelihood function')
 -fval

%% Part 4. Estimating all the parameters (Monte-Carlo)

% create standard normal variables first, add mean and standard deviation
% inside MC_general_C function
ub=randn(100,2,n);
x0=[3, 1, 0.1, 1, 0.1, 1];


options5 = optimset('Display','iter','algorithm','interior-point','InitBarrierParam',1, 'TolFun', 1e-12,'TolX',1e-12);
[P_all, fval, exitflag, output]=fmincon(@(par) MC_general_C(par, X, Y, Z, ub), x0,[],[],[],[],[-inf; 0; -inf; -inf; -inf; 0],[],@(par) mycon(par),options5);
  display('(Monte-Carlo) Full ML Estimation')
  display('Starting point')
  x0
  display('argmax:')
  display('beta_0, sigma_beta, gamma, u_0, sigma_beta_u, sigma_u=')
  P_all
  display('maximized value of loglikelihood function')
  -fval




