function [Like]= MC_general_B(par, u, X, Y, Z, BET)

beta_0=par(1);
sigma_beta=par(2);
gamma=par(3);
Func=@(x)(1+exp(-x)).^(-1);

L_i=@(beta, u, gamma) prod((Func(ones(size(X,1),1)*beta.*X+gamma*Z+ones(size(X,1),1)*u)).^Y.*(1-Func(ones(size(X,1),1)*beta.*X+gamma*Z+ones(size(X,1),1)*u)).^(1-Y),1);

n=100;
L_int=zeros(n,100);
BETA=beta_0*ones(n,100)+(sigma_beta)^(1/2)*BET;

for i=1:n
L_int(i,:)=(L_i(BETA(i,:), u, gamma));
end
   
Like=-sum(log(mean(L_int,1)));  
end