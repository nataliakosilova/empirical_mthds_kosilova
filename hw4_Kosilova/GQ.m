function [Like]= GQ(par, X, Y, Z, u, nodes ,w)

Func=@(x)(1+exp(-x)).^(-1);
beta_0=par(1);
sigma_beta=par(2);
gamma=par(3);

L_int=zeros(20,100);
% changing variables of GaussHermite nodes for normal distributon leads to
beta=2^(1/2)*sqrt(sigma_beta)*nodes'+ones(1,20)*beta_0;

L_i=@(beta, u, gamma) prod((Func(ones(20,100)*beta.*X+gamma*Z+ones(size(X,1),1)*u)).^Y.*(1-Func(ones(20,100)*beta.*X+gamma*Z+ones(size(X,1),1)*u)).^(1-Y),1);
for i=1:20  
L_int(i,:)=(L_i(beta(i), u, gamma));
end

% I calculate log likelihod here, because likelihood function takes values
% too close to zero
Like=-sum(log((pi)^(-1/2)*L_int'*w));
      
end