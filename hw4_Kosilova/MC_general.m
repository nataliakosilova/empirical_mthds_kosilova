function [Like]= MC_general(par, u, X, Y, Z)

Func=@(x)(1+exp(-x)).^(-1);
beta_0=par(1);
sigma_beta=par(2);
gamma=par(3);
L_i=@(beta, u, gamma) prod((Func(ones(size(X,1),1)*beta.*X+gamma*Z+ones(size(X,1),1)*u)).^Y.*(1-Func(ones(size(X,1),1)*beta.*X+gamma*Z+ones(size(X,1),1)*u)).^(1-Y),1);

% Generate n draws from N(beta_0, sigma_beta)
n=100;
L_int=zeros(n,100);
beta=beta_0*ones(n,100)+(sigma_beta)^(1/2)*randn(n,100);

for i=1:n
L_int(i,:)=(L_i(beta(i,:), u, gamma));
end
   
Like=-sum(log(mean(L_int,1)));
   
   
end