function [theta, var] = gamma_ml(X, in_point )
%ML for gamma distribution,
%returns estimated parameters and var-covar matrix 

Y_1=mean(X);
Y_2=exp(mean(log(X)));
var=zeros(2,2);
 f = @(a) log(a)-log(Y_1/Y_2)-psi_CE(a);
 

%As derivative of our function has closed form representation, one can try
%to implement Newton-Rapson procedure

df= @(a) 1/a-trigamma_CE(a);
x_new=in_point;

eps=0.0001;
maxsteps=1000;
for t=1:maxsteps
     x_old=x_new;
    x_new=x_old-f(x_old)/df(x_old);
    
    %a little fix in order to not to go away to the negative half-space
    %if someone start far away from 0, the trigamma function will be
    %undefined, the piece below helps to avoid this.
    if x_new<0
          if -x_new>x_old
        x_new=-x_old/x_new;
          else 
        x_new=-x_new/x_old;
          end
    else 
    
    if norm(x_new-x_old)<eps
    flag=0;
    theta(1)=x_new;
    theta(2)=theta(1)/Y_1;
    %computing variance-covariance matrix
    sigma11=mean((log(theta(2))+log(X)-psi_CE(theta(1))).^2);
    sigma12=mean((log(theta(2))+log(X)-psi_CE(theta(1))).*(theta(1)/theta(2)-X));
    sigma22=mean((theta(1)/theta(2)-X).^2);
    var=inv([sigma11, sigma12; sigma12, sigma22]);
    
    
    
    return
    end
    
    end
    
end

  
if t==maxsteps
    flag=1
    warning('Failure to converge: NR method');
    
end



end

