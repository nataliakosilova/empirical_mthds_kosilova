% Empirical Methods, HW3
% By Natalia Kosilova
%Last update: November 5

%Problem 1
clear all;


%% Parts a)-c)
display ('Part a)-c)')
% Auxiliary function gamma_ml in hw3 folder takes vector of X and a
% starting point as an input and returns likelihood estimates of thetas and
% their covariance matrix.

% let's choose arbitrary starting value
in_point=1.5;

% and create vector of X with known Gamma distribution to check whether the
% ML estimation works.
% Note that the MAtlab gamma distribution requires parameter a, 1/b as
% input.

display('Lets the true parameters be')
a=1
b=10

G=gamrnd(a,1/b,1,1000);

display('Estimatd parameters and their covariance matrix:')
[theta,var] = gamma_ml(G, in_point)



%% Part d)
% Plotting the theta_1 as a function of Y_1/Y_2
% I slightly change gamma_ml function (to gamma_ml_version2), so that instead of vector X it takes
% Y_1/Y_2 as input and returns parameter theta_1 only. 
% you did not have to do part D
step=0.01;
Space=1.1:step:3;
n=length(Space);

theta_pl=zeros(1,n);

for i=1:n
    theta_pl(i)=gamma_ml_v2(Space(i));
end


plot(Space, theta_pl)


    