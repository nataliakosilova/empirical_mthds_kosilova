function [theta] = gamma_ml_v2(R)
%ML for gamma distribution,
%returns value of theta_1


 f = @(a) log(a)-log(R)-psi_CE(a);
 

%As derivative of our function has closed form representation, one can try
%to implement Newton-Rapson procedure

df= @(a) 1/a-trigamma_CE(a);
x_new=1.5;

eps=0.0001;
maxsteps=1000;
for t=1:maxsteps
     x_old=x_new;
    x_new=x_old-f(x_old)/df(x_old);
    
    %a little fix in order to not to go away to the negative half-space
    %if someone start far away from 0, the trigamma function will be
    %undefined, the piece below helps to avoid this.
    if x_new<0
          if -x_new>x_old
        x_new=-x_old/x_new;
          else 
        x_new=-x_new/x_old;
          end
    else 
    
    if norm(x_new-x_old)<eps
    flag=0;
    theta=x_new;
    return
    end
    
    end
    
end

  
if t==maxsteps
    flag=1
    warning('Failure to converge: NR method');
    
end



end