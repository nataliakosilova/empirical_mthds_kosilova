function [g,hess] = nlls( y, X,beta)
%Auxiliary function for NLLS ML in problem2, HW3
%Takes observations y and X as input
%And returns gradient and 'approximate' Hessian at a given parameter value

n=length(y);
lambda=exp(X*beta);

Lambda=[lambda'; lambda'; lambda'; lambda'; lambda'; lambda']';
Y=[y'; y'; y'; y'; y'; y']';

dh=X.*Lambda;

hess=1/n*dh'*dh;

g=-1/n*sum(dh.*(Y-Lambda))';

   
end