function [f,g] = ML( y, X, beta)
%Auxiliary function for ML in problem2, HW3

% Given the parameters and data, compute the value of the maximum
% likelihood function for Poisson and it's derivative
lambda=exp(X*beta)


L=(-lambda+y.*(X*beta));
f=-sum(L)

Lambda=[lambda'; lambda'; lambda'; lambda'; lambda'; lambda']';
Y=[y'; y'; y'; y'; y'; y']';

der=X.*Lambda-X.*Y;

g=sum(der);

end

