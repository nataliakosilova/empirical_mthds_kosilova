function [g,hess] = bhhh( y, X,beta)
%Auxiliary function for BHHH ML in problem2, HW3
%Takes observations y and X as input
%And returns gradient and 'approximate' Hessian at a given parameter value
n=length(y);

lambda=exp(X*beta);

Lambda=[lambda'; lambda'; lambda'; lambda'; lambda'; lambda']';
Y=[y'; y'; y'; y'; y'; y']';

der=X.*Lambda-X.*Y;
g=1/n*sum(der)';

hess=1/n*der'*der;

    




end
