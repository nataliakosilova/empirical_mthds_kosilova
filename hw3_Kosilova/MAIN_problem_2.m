% Empirical Methods, HW3
% By Natalia Kosilova
%Last update: November 5

%Problem 2
%% Importing Data
clear all;

diary RESULTS_problem2.txt

A=importdata('hw3.mat');
y=A.y(:,1);
X=A.X(:,1:6);


%% Part 1
display('Part 1')
% For this part I wrote ML function (ML.m file), that takes as an input vectors of observations X and Y 
%and computes maximum likelihood function value at a given point beta and it's derivative 


% set up a starting point
x0=[0,0,0,0,0,0]';


%% FMINUNC without a derivative supplied
display('FMINUNC without a derivative supplied')
diary off
[P_fminunc, fval, exitflag, output]=fminunc(@(beta) ML(y, X, beta), x0);
diary on
P_fminunc
display('The number of iterations and function evaluations fminunc without the derivative supplied method:')
iter_fminunc=output.iterations
eval_fminunc=output.funcCount

%% FMINUNC with a derivative supplied
display('FMINUNC with a derivative supplied')
options = optimoptions('fminunc','GradObj','on');
diary off
[P_fminunc_grad, fval, exitflag, output]=fminunc(@(beta) ML(y, X, beta), x0, options);
diary on
P_fminunc_grad
display('The number of iterations and function evaluations fminunc with the derivativr method:')
iter_fminunc_grad=output.iterations
eval_fminunc_grad=output.funcCount

%% Neilder Mead. fminsearch -- standard matlab function implements this
% algorithm. 
display('Neilder Mead')

diary off
[P_NM, fval, exitflag, output]=fminsearch(@(beta) ML(y, X, beta), x0);
% this really does not seem to work as it should
diary on
P_NM
display('The number of iterations and function evaluations NM method:')
iter_NM=output.iterations
eval_NM=output.funcCount


%NM that starts from vector of zeros perform really badly. But it works
%well if the starting point is slightly bigger
x_nm=[0.1, 0.1, 0.1, 0.1, 0.1, 0.1]';

diary off
[P_NM_not0, fval, exitflag, output]=fminsearch(@(beta) ML(y, X, beta), x_nm);
diary on
P_NM_not0
display('The number of iterations and function evaluations NM method that starts not from 0:')
iter_NM_not0=output.iterations
eval_NM_not0=output.funcCount


%% BHHH MLE
display('BHHH MLE')
maxsteps=1000;
eps=1e-4;
% never make tolerance this coarse, I changed it to the default value for
% fminunc and it gives the same results.
x_old=x0;
flag=1;
for t=1:maxsteps
    [g,hess]=bhhh( y, X,x_old);
    x_new=x_old-hess^(-1)*g;
    if norm(x_new-x_old)<eps
        P_BHHH=x_new;
        flag=0;
        iter_BHHH=t;
        %as for each new iteratin we calculate the value of 'approximate'
        %hessian 
        eval_BHHH=t;
        break
    else x_old=x_new;
    end 
end
   if flag==1
       display('BHHH method:failure to succeed')
   end
   display('Estimated parameters and number of iterations: BHHH method')
   P_BHHH
   iter_BHHH
  
   
   
   
%% Part2
display('Part 2')
[c,hess0]=bhhh( y, X,x0);
[c,hess_final]=bhhh(y, X, P_BHHH);

display('Eigenvalues of the Hessian approximation for the initial Hessian and the Hessian at the estimated parameters:')
e_0=eig(hess0)
e_f=eig(hess_final)



%% Part3 
display('Part 3')
display('Non linear least squares method')


%this is how the usual NLLS procedure looks like. Instead of fminunc any of
%the standard minimization procedures could be used.
% rr=@(beta) sum((y-exp(X*beta)).^2);
% [P_NNLS, fval, exitflag, output]=fminunc(@(beta) rr(beta), x0);

%But we are writing the more efficinet (probably) algorithm.



maxsteps=1000;
eps=0.001;
x_old=x0;
flag=1;
for t=1:maxsteps
    [g,hess]=nlls( y, X,x_old);
    x_new=x_old-hess^(-1)*g;
    if norm(x_new-x_old)<eps
        P_NLLS=x_new;
        flag=0;
        iter_NLLS=t;
        %as for each new iteratin we calculate the value of 'approximate'
        %hessian 
        eval_NLLS=t;
        break
    else x_old=x_new;
    end 
end
   if flag==1
       display('NLLS method:failure to succeed')
   end
   display('Estimated parameters and number of iterations: NLLS method')
P_NLLS
iter_NLLS

%% Part 4
display('Part 4')
%Computiong standard errors for BHHH and NLLS methods

%For BHHH we already now covariance matrix, it's our inverted final Hessian
cov_BHHH=inv(hess_final)


%For NLLS var-covar matrix we need to compute one additional term
n=length(y);
[c,H]=nlls(y,X,P_NLLS);
lambda=exp(X*P_NLLS);

Lambda=[lambda'; lambda'; lambda'; lambda'; lambda'; lambda']';
Y=[y'; y'; y'; y'; y'; y']';

dh=X.*Lambda.*(Y-Lambda);

sigma=1/n*(dh')*dh;
cov_NLLS=H^(-1)*sigma*H^(-1)


diary off














